# TootBot

A small python script to replicate tweets on a mastodon account.
This is a fork of [cquest's tootbot](https://github.com/cquest/tootbot) that
uses Twitter's API instead of using RSS scrapers.

The script needs Mastodon login/pass to post toots, as well as a set of Twitter
application credentials (see
https://developer.twitter.com/en/docs/basics/authentication/guides/access-tokens).

## Features

- Twitter tracking links (t.co) are dereferenced
- Twitter-hosted pictures, GIFs and videos are retrieved and uploaded to
  Mastodon if they are less than 5 MB big. If there are several variants of a
  video, the script picks the best-quality variant that is smaller than 5 MB. If
  all variants are too big, the video is not uploaded and a link to the tweet is
  left instead.
- Twitter users' handles are replaced with their long names
- Retweets are signaled with a bird emoji! 🐦

An sqlite database is used to keep track of tweets than have been tooted.


The simplest way to use the script is to simply make it run as a cron job on any
server (not necessarily the Mastodon instance server).
